### FishApp is application for managing data about zebrafish for genetic experiments. It anages data about fish stock, its loacation, counts etc.

FishApp is designed to use Python 3.7+ and Django 2.2

## clone
clone the repository 

git clone https://ANo34@bitbucket.org/ANo34/zf_project.git

## requirements
django 2.2
Python 3.7+
django-tables2


## run installation script
Installation script will run migrations, create new database and load it with sample data from db.json file

    ./runinstall
 
 or for distributions with pre-installed python2   

    ./runinstall_p3 


## start the local django server

    ./runserver
    
 or for distributions with pre-installed python2   
 
    ./runserver_p3
 

## start browser
application will open with your default browser at localhost, you will be asked to log in   

    ./startbrowser


