import django_tables2 as tables
from .models import Rack

class RackTable(tables.Table):
    class Meta:
        model = Rack
        # template_name = 'racks2'

