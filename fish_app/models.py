from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Q

class Fishline(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Room(models.Model):
    room = models.CharField(max_length=15)

    def __str__(self):
        return self.room

class FilledPositionsManager(models.Manager):
    def filled(self):
        return Position.objects.filter(substock=True)

class Rack(models.Model):
    rackname = models.CharField(max_length=20, unique=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    row_count = models.PositiveIntegerField(null=True)
    col_count = models.PositiveIntegerField(null=True)


    def __str__(self):
        return self.rackname

    def get_row_range(self):
        row_range = range(1, (self.row_count+1))
        return row_range

    def get_col_range(self):
        col_range = range(1, (self.col_count+1))
        return col_range


class Position(models.Model):

    rack = models.ForeignKey(Rack, on_delete=models.CASCADE)
    row = models.IntegerField(null=True)
    column = models.IntegerField(null=True)

    objects = FilledPositionsManager()

    objects = models.Manager()

    def __str__(self):
        return "room: %s, rack: %s, row: %s, column: %s" % (self.rack.room, self.rack.rackname, self.row, self.column)

    def position_filled(self):
        return self.substock


class Stock(models.Model):
    fishline = models.ForeignKey(Fishline, on_delete=models.CASCADE)
    d_o_b = models.DateField(max_length=12)

    def __str__(self):
        return "{}, {}".format(self.fishline, self.d_o_b)

class Substock(models.Model):
    substock_id = models.AutoField(primary_key=True)
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    position = models.OneToOneField(Position, on_delete=models.CASCADE)
    count = models.PositiveIntegerField(null=True)

    def __str__(self):
        return "Sub of {}".format(self.stock.fishline)


# creates Poisitons objects for each slot in a rack automatically after rack object creation
# adds / deletes positions after edit of rack
@receiver(post_save, sender=Rack)
def create_positions(sender, instance, created, update_fields=[], **kwargs):
        row_range = range(1, (instance.row_count + 1))
        col_range = range(1, (instance.col_count + 1))
        for x in row_range:
            for y in col_range:
                found = Position.objects.filter(rack=instance, row=x, column=y)
                if not found:
                    Position.objects.create(rack=instance, row=x, column=y)
        # checks for redundant positions in case rows/columns in rack were edited and adds or deletes them
        for position in (Position.objects.all()):
            if position.row not in row_range:
                position.delete()
            if position.column not in col_range:
                position.delete()













