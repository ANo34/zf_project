from django.views import View
from django.views.generic import TemplateView, DetailView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import AddRackForm
from.tables import RackTable
from django.urls import reverse



from .models import Room, Rack, Position, Substock, Stock, Fishline

class MultipleModelView(TemplateView):
    template_name = 'fish_app/index.html'

    q = Position.objects.filter(substock__isnull=False)

    def get_context_data(self, **kwargs):
        context = super(MultipleModelView, self).get_context_data(**kwargs)
        context['rooms'] = list(Room.objects.all())
        context['racks'] = list(Rack.objects.all())
        context['filled'] = Position.objects.filter(substock__isnull=False)
        context['positions'] = Position.objects.all()
        context['substock'] = list(Substock.objects.all())

        return context

class RackDetail(DetailView):
    template_name = 'fish_app/rack.html'

    model = Rack

    def get_context_data(self, **kwargs):
        context = super(RackDetail, self).get_context_data(**kwargs)

        return context

class SubstockDetail(LoginRequiredMixin, DetailView):
    template_name = 'fish_app/substock.html'

    model = Substock

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class StockDetail(DetailView):
    template_name = 'fish_app/stock.html'

    model = Stock

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class FishlineDetail(DetailView):
    template_name = 'fish_app/fishline.html'

    model = Fishline

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class Racks1(ListView):
    template_name = 'fish_app/racks1.html'

    model = Rack

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class Racks2(TemplateView):
    template_name = 'fish_app/racks2.html'

    model = Rack

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        table = RackTable(Rack.objects.filter(pk__gte=1))
        context['table'] = table

        return context


class Rooms(ListView):
    template_name = 'fish_app/rooms.html'

    model = Room

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

class FishlinesList(ListView):
    template_name = 'fish_app/fishlines.html'

    model = Fishline

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

def filled(request):

    filled_positions = Position.objects.filled()

    return render(request, 'fish_app/filled.html', {'filled':filled_positions})

def addtorack(request):

    # chosen_substock = Substock.objects.get(pk=request.POST['substock_to_fill')

    response = "This means success and %s" % request.POST

    return HttpResponseRedirect('add_to_rack')

def add_rack(request):

    if request.method == 'POST':
        form = AddRackForm(request.POST)

        if form.is_valid():
            return HttpResponse('thank you')
    else:
        form = AddRackForm()

    return render(request, 'fish_app/add_rack.html', {'form2': form})

def thanks(request):
    return HttpResponse('thank you')

# def login(request):
#     pass
#
# def index(request):
#     return render(request, 'zebrafish/index.html')
