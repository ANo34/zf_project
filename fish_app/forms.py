from django.forms import ModelForm
from .models import Rack

class AddRackForm(ModelForm):
    class Meta:
        model = Rack
        fields = ['rackname', 'room', 'row_count', 'col_count']