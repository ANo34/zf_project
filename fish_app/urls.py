from django.urls import path

from .views import MultipleModelView, RackDetail, SubstockDetail, StockDetail, FishlineDetail, Racks1, Rooms, FishlinesList, filled, Racks2
from . import views


urlpatterns = [
    path('', MultipleModelView.as_view()),
    path('rack/<int:pk>', RackDetail.as_view(), name='rack'),
    path('available_racks', Racks1.as_view(), name='racks1'),
    path('available_racks2', Racks2.as_view(), name='racks2'),
    path('rooms', Rooms.as_view(), name='rooms'),
    path('substock/<int:pk>', SubstockDetail.as_view(), name='substock'),
    path('stock/<int:pk>', StockDetail.as_view(), name='stock'),
    path('fishline/<int:pk>', FishlineDetail.as_view(), name='fishline'),
    path('fishlines', FishlinesList.as_view(), name='fishlineslist'),
    path('filled', filled, name='filled'),
    path('add_to_rack', views.addtorack, name='fill_pos'),
    path('add_rack', views.add_rack, name='add_rack'),
    # path('thanks', views.thanks, name='thanks'),

]